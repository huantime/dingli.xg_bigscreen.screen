import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import axios from 'axios'
import eventBus from 'vue3-eventbus'
import services from './services'


import 'element-plus/lib/theme-chalk/index.css'

const app = createApp(App)

app.config.globalProperties.$services = services
app.config.globalProperties.$axios=axios

app
    .use(ElementPlus)
    .use(services)
    .use(eventBus);
    
app.mount('#app')
