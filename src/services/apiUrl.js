/**
 *  api接口地址汇总
 */

export const apiUrl = {
    'testSugars': '/api/test/sugars',

    'login': '/api/login',
    'loginOut': '/api/loginOut',

    // mock
    'qryPageConfig': '/110/zlaq',

    'msg': '/index/securityPolicy'
}
